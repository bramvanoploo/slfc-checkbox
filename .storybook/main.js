module.exports = {
  "stories": [
    "../projects/slfc-checkbox/src/app/**/*.stories.mdx",
    "../projects/slfc-checkbox/src/app/**/*.stories.@(js|jsx|ts|tsx)"
  ],
  "addons": [
    "@storybook/addon-actions",
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-docs",
    "@storybook/addon-events",
    "@storybook/addon-jest",
    "@storybook/addon-storysource",
    "@storybook/addon-viewport",
    "@storybook/addon-knobs"
  ]
}
