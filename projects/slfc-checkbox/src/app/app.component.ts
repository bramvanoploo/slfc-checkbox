import { Component, DoBootstrap, Injector } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { EbCheckboxComponent } from './eb-checkbox/eb-checkbox.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements DoBootstrap {

  constructor(private injector: Injector) {
    const webComponent = createCustomElement(EbCheckboxComponent, {injector});
    customElements.define('eb-checkbox', webComponent);
  }

  ngDoBootstrap(): void {}
}
