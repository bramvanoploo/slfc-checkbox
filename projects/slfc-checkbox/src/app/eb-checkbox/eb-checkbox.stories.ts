import { action } from '@storybook/addon-actions';
import { boolean, text, withKnobs } from '@storybook/addon-knobs';
import { EbCheckboxComponent } from './eb-checkbox.component';

export default {
  title: 'eb-checkbox',
  excludeStories: /.*Data$/,
  decorators: [withKnobs]
};

export const AllInteractions = () => ({
  title: 'All interactions',
  component: EbCheckboxComponent,
  props: {
    name: text('name', 'Input name'),
    correct: boolean('correct', false),
    indeterminate: boolean('indeterminate', false),
    trueValue: boolean('trueValue', false),
    falseValue: boolean('falseValue', true),
    correctChange: action('correctChange')
  }
});
