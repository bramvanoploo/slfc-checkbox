import {
  Component,
  EventEmitter,
  forwardRef,
  HostBinding,
  Input,
  Output
} from '@angular/core';
import {
  ControlValueAccessor,
  NG_VALUE_ACCESSOR
} from '@angular/forms';

/**
 * The EbCheckboxComponent
 *
 * Example of usage:
 * <example-url>https://efc.edubase.sanomalearning.com/frontend/demo/3.x/master/#/demo/checkbox</example-url>
 */
@Component({
  providers: [
    {
      multi: true,
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => EbCheckboxComponent)
    }
  ],
  selector: 'app-eb-checkbox',
  templateUrl: './eb-checkbox.component.html'
})
export class EbCheckboxComponent implements ControlValueAccessor {
  @HostBinding('class.eb-checked') checked: boolean;

  @Input() name: string;
  @Input() correct?: boolean;
  @HostBinding('attr.disabled') disabled: boolean;
  @Input() @HostBinding('class.eb-indeterminate') indeterminate: boolean;
  @Input() trueValue: any = true;
  @Input() falseValue: any = false;

  @Output() correctChange = new EventEmitter<boolean>();

  private onTouched: () => void;
  private onChange: (_: any) => void;

  get icon(): string {
    if (this.indeterminate) {
      return 'minus';
    } else if (this.correct === false && this.checked) {
      return 'incorrect';
    } else if (this.correct === true && this.checked) {
      return 'correct';
    } else if (this.checked) {
      return 'check';
    } else {
      return '';
    }
  }

  get result(): string {
    if (this.correct && this.checked) {
      return 'correct';
    } else if (this.correct && !this.checked) {
      return 'missing';
    } else if (this.correct === false && this.checked) {
      return 'incorrect';
    } else {
      return '';
    }
  }

  toggle(event: Event): void {
    // This function ensures that the checkbox also works when not embedded in a label element
    // As the input element is set to display:none it cannot be clicked otherwise
    event.preventDefault();

    if (!this.disabled) {
      if (this.indeterminate) {
        this.indeterminate = false;
      }

      this.checked = !this.checked;

      this.valueChanged();
    }
  }

  writeValue(value: any): void {
    this.checked = value === this.trueValue;
  }

  registerOnChange(callback: (_: any) => void): void {
    this.onChange = callback;
  }

  registerOnTouched(callback: () => void): void {
    this.onTouched = callback;
  }

  setDisabledState(disabled: boolean): void {
    this.disabled = disabled;
  }

  valueChanged(): void {
    this.correct = undefined;
    this.correctChange.emit(this.correct);

    this.onTouched();
    this.onChange(this.checked ? this.trueValue : this.falseValue);
  }
}
